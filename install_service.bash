#! /bin/bash

sudo cp ./auth.service /lib/systemd/system/auth.service
sudo chmod u+x /home/ubuntu/TheologyGoats/run_auth_prod.bash
sudo systemctl enable auth
sudo systemctl restart auth

sudo cp ./theology-goats.service /lib/systemd/system/theology-goats.service
sudo chmod u+x /home/ubuntu/TheologyGoats/run_server_prod.bash
sudo systemctl enable theology-goats
sudo systemctl restart theology-goats
