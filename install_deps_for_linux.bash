#! /bin/bash

wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb

sudo add-apt-repository universe
sudo apt -y update

sudo apt install -y apt-transport-https python3 python3-pip python3-venv nginx

sudo apt -y update
sudo apt install -y dotnet-sdk-6.0
