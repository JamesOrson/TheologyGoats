#! /bin/bash

SRC_DIR=/home/ubuntu/TheologyGoats

sudo chmod 777 /var/log

cd $SRC_DIR/app
npm install
npm run build

cd $SRC_DIR
bash ./install_deps_for_linux.bash

cd $SRC_DIR/server
bash ./build_server.bash

sites_available_conf=/etc/nginx/sites-available/goats-nginx.conf
cd $SRC_DIR/nginx
sudo rm -f $sites_available_conf
sudo ln -f ./goats-nginx.conf $sites_available_conf

sudo rm -f /etc/nginx/sites-enabled/goats-nginx.conf
sudo ln -s $sites_available_conf /etc/nginx/sites-enabled
sudo systemctl restart nginx

cd $SRC_DIR
bash ./install_service.bash
sudo systemctl daemon-reload

